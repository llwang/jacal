## JACAL
**J**oint **A**stronomy **CAL**ibration and
imaging software

JACAL integrates [ASKAPSoft](https://www.atnf.csiro.au/computing/software/askapsoft/sdp/docs/current/pipelines/introduction.html)
(now rebranded to *Yandasoft*)
and the execution framework [DALiuGE](https://github.com/ICRAR/daliuge).
A shared library offers a calling convention supported by DALiuGE and internally links and reuses ASKAPSoft code.
JACAL is freely available in this [GitLab repository](https://gitlab.com/ska-telescope/jacal)
under a variation of the open source BSD 3-Clause [License](LICENSE).
The repository contains the following:

* The C/C++ code of the shared library libaskapsoft_dlg.so described above
* A standalone utility for library testing independent of DALiuGE.

This repository is an offshoot from the original located in [GitHub](https://github.com/ICRAR/jacal).
The latter should be considered deprecated, and has only been left available
for reference.

## Acknowledgement

The development of JACAL was jointly initiated by [ICRAR](https://www.icrar.org/) and [CSIRO](https://www.csiro.au/) during the SKA pre-construction phase. The work was supported by the Australian Government through the Department of Industry, Innovation and Science under the Round 2 SKA Pre-construction Grants Programme and more recently by the SKA Bridging Grant SKA75656.


## Questions?

Feel free to open an issue to discuss any questions not covered so far.

## DoxyGen

1, git remote set-url origin THE_CLONE_URL

2, run "GITLAB_REPO=$(git remote -v) PROJECT_NUMBER=$(git rev-parse --short HEAD) doxygen"

3, test in Windows and  Linux OS

4, run "xsltproc combine.xslt index.xml >all.xml" to combine all the xml files to one file
