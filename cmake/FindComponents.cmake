# 
# This is the Find file for the casa component sub-module. This lives in the casarest package
#
# Variables used by this module:
#  COMPONENTS_ROOT_DIR     - COMPONENTS root directory
# Variables defined by this module:
#  COMPONENTS_FOUND - system has COMPONENTS
#  COMPONENTS_INCLUDE_DIR  - the COMPONENTS/include directory (cached)
#  COMPONENTS_INCLUDE_DIRS - the COMPONENTS include directories
#                          (identical to COMPONENTS_INCLUDE_DIR)
#  COMPONENTS_LIBRARY      - the COMPONENTS  library (cached)
#  COMPONENTS_LIBRARIES    - the COMPONENTS library plus the libraries it 
#                          depends on

# Copyright (C) 2019


if(NOT COMPONENTS_FOUND)

	find_path(COMPONENTS_INCLUDE_DIR casarest/components/ComponentModels.h 
		HINTS ${COMPONENTS_ROOT_DIR} PATH_SUFFIXES include/ )
	find_library(COMPONENTS_LIBRARY casa_components
		HINTS ${COMPONENTS_ROOT_DIR} PATH_SUFFIXES lib)

	set(COMPONENTS_INCLUDE_DIRS ${COMPONENTS_INCLUDE_DIR} ${COMPONENTS_INCLUDE_DIR}/casarest )
	set(COMPONENTS_LIBRARIES ${COMPONENTS_LIBRARY})
        if(CMAKE_VERSION VERSION_LESS "2.8.3")
		find_package_handle_standard_args(COMPONENTS DEFAULT_MSG COMPONENTS_LIBRARY COMPONENTS_INCLUDE_DIR)
        else ()
	   include(FindPackageHandleStandardArgs)
	   find_package_handle_standard_args(COMPONENTS DEFAULT_MSG COMPONENTS_LIBRARY COMPONENTS_INCLUDE_DIR )
        endif ()
	set(Components_FOUND TRUE)


endif(NOT COMPONENTS_FOUND)
