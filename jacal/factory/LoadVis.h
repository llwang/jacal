/*!
*  \file LoadVis.h 
*  \brief Load a CASA Measurement Set in the DaliugeApplication Framework
*  \details We will build on the LoadParset structure - but use the contents
*  of the parset to load a measurement set. 
*  \EAGLE
*  \EAGLE_PARAM param/gitrepo $(GITLAB_REPO)
*  \EAGLE_PARAM param/version $(PROJECT_NUMBER)
*  \EAGLE_PARAM param/category 
* 		\~English this is DynlibApp\n
* 		\~Chinese Dynlib程序\n
* 		\~ 
* \EAGLE_END
*/


#ifndef ASKAP_FACTORY_LOADVIS_H
#define ASKAP_FACTORY_LOADVIS_H

#include "rename.h"

#include <daliuge/DaliugeApplication.h>

#include <casacore/casa/Quanta/MVDirection.h>

#include <boost/shared_ptr.hpp>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// ASKAPSoft data accessor
#include <dataaccess/TableDataSource.h>

#include <fitting/Params.h>


/*!
 * \EAGLE
 * \EAGLE_PARAM[in] param/description 
 *		\~English this is a init function\n
 *		\~Chinese 初始化函数\n
 *		\~ 
 * \EAGLE_PARAM[in] param/start_frequency 
 *		\~English the start frequency to read from\n
 *		\~Chinese 要读取的起始频率\n
 *		\~ 
 * \EAGLE_PARAM[in] param/end_frequency 
  *		\~English the end frequency to read from\n
 *		\~Chinese 要读取的结束频率\n
 *		\~ 
 * \EAGLE_PARAM[in] param/channels 
 *		\~English how many channels to load\n
 *		\~Chinese 需要加载的通道数量\n
 *		\~
 * \EAGLE_PARAM[in] port/config
 *		\~English the configuration of the input_port\n
 *		\~Chinese 输入端口的设置\n
 *		\~
 * \EAGLE_PARAM[in] port/event
 *		\~English the event of the input_port\n
 *		\~Chinese 输入端口的事件\n
 *		\~
 * \EAGLE_PARAM[in] local-port/event
 *		\~English the event of the input_port\n
 *		\~Chinese 输入端口的事件\n
 *		\~
 * \EAGLE_PARAM[out] port/File
 *		\~English the file of the output_port \n
 *		\~Chinese 输出端口的文件\n
 *		\~
 * \EAGLE_END
 */

namespace askap {
  /// @brief Loads visibility set
  /// @details Loads a configuration from a file drop and a visibility set from a casacore::Measurement Set
    class LoadVis : public DaliugeApplication

    {

    public:

        typedef boost::shared_ptr<LoadVis> ShPtr;

        LoadVis(dlg_app_info *raw_app);

        static inline std::string ApplicationName() { return "LoadVis";}

        virtual ~LoadVis();

        static DaliugeApplication::ShPtr createDaliugeApplication(dlg_app_info *raw_app);

        virtual int init(const char ***arguments);

        virtual int run();

        virtual void data_written(const char *uid, const char *data, size_t n);

        virtual void drop_completed(const char *uid, drop_status status);

        static std::vector<std::string> getDatasets(const LOFAR::ParameterSet& parset);


        private:

            /// The model
            askap::scimath::Params::ShPtr itsModel;

            // Parameter set
            LOFAR::ParameterSet itsParset;

            // Its channel of data

            casacore::IPosition freqInterval;


            casacore::IPosition timeInterval;

            // Its tangent point
            std::vector<casacore::MVDirection> itsTangent;

            int itsChan;


            // utility to build an Imaging Normal Equation from a parset
            // void buildNE();

            // these are the steps required by buildNE




    };

} // namespace askap


#endif //
